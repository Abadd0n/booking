﻿using System;
using System.Collections.Generic;

namespace Booking.Models
{
    public partial class Visitor
    {
        public Visitor()
        {
            Reservation = new HashSet<Reservation>();
        }

        public int VisitorId { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string EMail { get; set; }
        public string Password { get; set; }

        public virtual ICollection<Reservation> Reservation { get; set; }
    }
}
