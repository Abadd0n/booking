﻿using System;
using System.Collections.Generic;

namespace Booking.Models
{
    public partial class Reservation
    {
        public int ReservationId { get; set; }
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public int NumberOfGuests { get; set; }
        public int VisitorId { get; set; }
        public int RoomId { get; set; }

        public virtual Room Room { get; set; }
        public virtual Visitor Visitor { get; set; }
    }
}
