﻿using System;
using System.Collections.Generic;

namespace Booking.Models
{
    public partial class Room
    {
        public Room()
        {
            Reservation = new HashSet<Reservation>();
        }

        public int RoomId { get; set; }
        public int RoomNumber { get; set; }
        public int Floor { get; set; }
        public string Type { get; set; }
        public int Capacity { get; set; }
        public int Price { get; set; }

        public virtual ICollection<Reservation> Reservation { get; set; }
    }
}
