﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Booking.Models;
using System.Net.Mime;
using System.Security.Cryptography.X509Certificates;
using Microsoft.EntityFrameworkCore;
using System.Reflection.Metadata.Ecma335;

namespace Booking.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ExcersiceController : ControllerBase
    {
        #region Context
        private readonly BookingDBContext _ctx;
        public ExcersiceController(BookingDBContext ctx)
        {
            _ctx = ctx;
        }
        #endregion
        #region Asynchronous methods
        [HttpGet("rooms")]
        public async Task<List<Room>> GetRoomsAsync() =>
            await _ctx.Room.ToListAsync();

        [HttpGet("rooms/{id}")]
        public async Task<Room> GetRoomAsync(int id) =>
            await _ctx.Room.FindAsync(id);

        [HttpPost("rooms")]
        public async Task<Room> InsertRoom(Room room)
        {
            room = new Room()
            {
                RoomNumber = room.RoomNumber,
                Floor = room.Floor,
                Type = room.Type,
                Capacity = room.Capacity,
                Price = room.Price
            };
            await _ctx.AddAsync(room);
            await _ctx.SaveChangesAsync();
            return room;
        }

        [HttpPut("rooms/{id}")]
        public async Task<ActionResult<Room>> UpdateRoom(int id, Room room)
        {
            if(id != room.RoomId)
            {
                return BadRequest();
            }

            var existingRoom = await _ctx.Room.FindAsync(id);

            existingRoom.RoomNumber = room.RoomNumber;
            existingRoom.Floor = room.Floor;
            existingRoom.Type = room.Type;
            existingRoom.Capacity = room.Capacity;
            existingRoom.Price = room.Price;

            try
            {
                await _ctx.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if(await _ctx.Room.AnyAsync(r => r.RoomId == id) == false)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return existingRoom;
            //return NoContent();
        }

        [HttpDelete("rooms/{id}")]
        public async Task<Room> DeleteRoom(int id)
        {
            Room room = await _ctx.Room.FindAsync(id);
            _ctx.Room.Remove(room);
            await _ctx.SaveChangesAsync();
            return room;
        }

        #region Visitors&Reservations
        [HttpGet("visitors")]
        public Task<List<Visitor>> GetVisitorsAsync() =>
            _ctx.Visitor.ToListAsync();

        [HttpGet("visitors/{id}")]
        public Task<List<Visitor>> GetVisitorAsync(int id) =>
            _ctx.Visitor.Where(r => r.VisitorId == id).ToListAsync();

        [HttpGet("reservations")]
        public Task<List<Reservation>> GetReservationsAsync() =>
            _ctx.Reservation.ToListAsync();

        [HttpGet("reservations/{id}")]
        public Task<List<Reservation>> GetReservationAsync(int id) =>
            _ctx.Reservation.Where(r => r.ReservationId == id).ToListAsync();
        #endregion
        #endregion
        #region Synchronous Methods
        //public List<Room> GetRooms() =>
        //    _ctx.Room.Select(r => r).ToList();
        //public List<Visitor> GetVisitors() =>
        //    _ctx.Visitor.Select(r => r).ToList();
        //public List<Reservation> GetReservations() =>
        //    _ctx.Reservation.Select(r => r).ToList();
        #endregion
    }
}
